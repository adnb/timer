#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tkinter
import os
import datetime

def alarme(self):
    """
    Fonction qui fait poper une alerte
    """ 
    self.topLevel = tkinter.Toplevel(self)
    self.topLevel.title = "DRIIIING!!"
    self.topLevel.grid()
    msg = tkinter.Label(self.topLevel, text="DRIIIINGG!!",bg="#002B36",fg="#93A1A1",font=("Monospace",100,))
    msg.grid(row=0,column=0)

    button = tkinter.Button(self.topLevel, text="OK", command=self.quit)
    button.grid(row=1,column=0)

def secondUpdate(self):
    """
    Fonction horloge qui change toutes les secondes
    """
    if self.pause == 0:
        self.secondes -= 1

        # Calcul
        if self.secondes == -1:
            self.minutes -= 1
            self.secondes = 59

        secondes = str(self.secondes)
        minutes = str(self.minutes)
        # Affichage
        if len(secondes) < 2:
            secondes = '0'+secondes
        if len(minutes) < 2:
            minutes = '0'+minutes
        self.monTexte["text"] = minutes+':'+secondes

    # Alarme
    if self.minutes == 0 and self.secondes == 0 and self.pause == 0:
        alarme(self)
    
    # After 1 second, update
    self.after(1000, secondUpdate, self)

def millisecondUpdate(self):
    # After 100 milliseconds, update
    #self.after(100, millisecondUpdate, self)
    pass

def hello():
    print("Hello!")

class Timer(tkinter.Tk):
    """
    Classe du timer
    """

    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        """
        Fonction d'initialisation
        """
        self.grid()

        # create a toplevel menu
        menuBar = tkinter.Menu(self)

        # create a pulldown menu, and add it to the menu bar
        fileMenu = tkinter.Menu(menuBar, tearoff=0)
        fileMenu.add_command(label="Hello", command=hello)
        fileMenu.add_command(label="Configurer", command=self.configurer)
        fileMenu.add_separator()
        fileMenu.add_command(label="Quitter", command=self.quit)
        menuBar.add_cascade(label="Fichier", menu=fileMenu)
        menuBar.add_command(label="Pause", command=self.pauseButtonClick)

        # display the menu
        self.config(menu=menuBar)

        # display time zone
        self.monTexte = tkinter.Label(self,height=5,width=20,bg="#002B36",fg="#93A1A1",font=("Monospace", 12,))
        self.monTexte.grid(row=0,column=0,columnspan=3)

        # display pause button
        self.pauseButton = tkinter.Button(self,text=u"Pause",command=self.pauseButtonClick)
        self.pauseButton.grid(column=0,row=1,sticky="W")
        self.pause = 0

        # display status bar
        #self.status = tkinter.StringVar()
        #self.statusBar = tkinter.Label(self,textvariable=self.status)
        #self.statusBar.grid(column=1,row=1,sticky="W")

        # display the timer
        #self.timer = tkinter.Label(self, text="0")
        #self.timer.grid(column=2,row=1,sticky='E')
        self.secondes = 0
        self.minutes = 0

        self.grid_columnconfigure(1,weight=1)
        self.resizable(True,False)
        self.update()
        self.geometry(self.geometry())        
        self.monTexte.focus_set()
        self.monTexte.bind('<Control-p>', self.pauseButtonClick)
        self.monTexte.bind('<Control-c>', self.configurer)
        #self.bind('<Control-q>', self.quit)

    def pauseButtonClick(self,event=None):
        """
        Clic sur le bouton pause
        """
        if self.pause == 1:
            self.pauseButton.config(relief="raised")
            self.pause = 0
        else:
            self.pauseButton.config(relief="sunken")
            self.pause = 1
        return "break"

    def fermeConfig(self,event=None):
        """
        Valide et ferme les configurations
        """
        try:
            self.secondes = int(self.configTopLevel.secondeEntry.get())
        except:
            self.secondes = 0
        try:
            self.minutes = int(self.configTopLevel.minuteEntry.get())
        except:
            self.minutes = 0
        self.pause = 0
        self.configTopLevel.destroy()

    def configurer(self,event=None):
        """
        Pop up pour configurer
        """
        self.configTopLevel = tkinter.Toplevel(self)
        self.configTopLevel.title = "Configuration"
        self.configTopLevel.grid()

        minuteLabel = tkinter.Label(self.configTopLevel,text="Minutes :")
        minuteLabel.grid(row=0,column=0)
        secondeLabel = tkinter.Label(self.configTopLevel,text="Secondes :")
        secondeLabel.grid(row=1,column=0)
        self.configTopLevel.minuteEntry = tkinter.Entry(self.configTopLevel)
        self.configTopLevel.minuteEntry.grid(row=0,column=1)
        self.configTopLevel.secondeEntry = tkinter.Entry(self.configTopLevel)
        self.configTopLevel.secondeEntry.grid(row=1,column=1)

        button = tkinter.Button(self.configTopLevel, text="OK", command=self.fermeConfig)
        button.grid(row=2,column=0,columnspan=2)

if __name__ == "__main__":
    root = Timer(None)
    root.title('Timer')

    # Launch the status message after 1 millisecond (when the window is loaded)
    root.after(1, secondUpdate, root)
    root.after(1, millisecondUpdate, root)

    root.pause = 1
    root.secondes = 10

    root.mainloop()
