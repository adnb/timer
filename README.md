# timer

A simple timer in python with tkinter

![Timer](doc/timer.gif)

The script uses tkinter, so it needs to be installed.
```
pip3 install tkinter
```

Then, just type:
```
python3 timer.py
```
